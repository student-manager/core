/*
 * core library for the student-manager application
 * Copyright (C) 2019 Koch, Ikari
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.mybaobab.studentmanager.model;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import java.util.HashSet;
import java.util.Set;

@Data
@Builder
public class SchoolClass {
    @NonNull
    private String name = "SCHOOL_CLASS_NO_NAME";
    @NonNull
    private Set<Student> students = new HashSet<>();

    public SchoolClass() {
    }
}

