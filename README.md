# student manager core library
this library provides core classes and functionality
which are shared between the windows and android projects of the student manager

## integration into the android project in intellij
 - add the lombok plugin to the android project (File -> Settings -> Plugins -> Marketplace -> "lombok")
 - clone the core project and open it
~~~
git clone git@gitlab.com:student-manager/core.git
~~~
 - add the lombok plugin to the core project
 - build the core
~~~
mvn clean install
~~~
copy the core jar into the android project
~~~
cp core.jar student-manager-android/lib/
~~~
right-click the core.jar and choose "Add as Library..."

## dependencies
### project lombok
#### description
"Project Lombok is a java library that automatically plugs into your editor and build tools,
spicing up your java. Never write another getter or equals method again, with one annotation
your class has a fully featured builder, Automate your logging variables, and much more." - projectlombok.org

#### purpose
we use lombok to dramatically reduce the amount of boilerplate code in the codebase.
with just a few annotations we create clean, concise and null-safe java-classes. 

#### documentation
https://projectlombok.org/features/all

#### license
MIT license (http://www.opensource.org/licenses/mit-license.php)